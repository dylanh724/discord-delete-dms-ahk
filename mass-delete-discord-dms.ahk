#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

^j::

Loop, 500
{
	if ( Mod(A_Index, 10) == 0 )
		send, {PgUp}
	else
		send, {Up}

	send, ^a
	send, {BS}
	send, {Enter}
	send, {Enter}
	sleep, 500
}
Return