# discord-delete-dms-ahk

Based on [ScBones306's Reddit post](https://www.reddit.com/r/discordapp/comments/4j4ogp/batch_deleting_dms/), this is an improved-upon script for Discord DM's that have more than ~20 to 50 message exchanges.

RazinhHei best describes how to setup this script with AHK (Auto HotKey): [(YouTube Video)](https://www.youtube.com/watch?v=7vYQATyqpos)

Get AHK @ https://www.autohotkey.com/

* Note that PINS and VIDEO CALLS seem to interrupt this - if anyone knows how to improve upon this, fork away!
* The script defaults to 500 DM's deleted per script run. Change this if you want. You can also pause the script by right clicking the "H" icon in the bottom-right desktop in the tray.
* This seems to only work in Windows